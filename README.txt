Overview
--------

FizzBuzz is a simple counting game.

The following implementations explore processing of the FizzBuzz game to 
much higher numbers and how to manage the performance of that.

Implementations
---------------

First is a simple test of an algorithm which counts to 1,000,000

Second is an attempt at 1,000,000,000 which uses a blocking queue
to store the output and writes in batches to the disk.

Third is the final attempt which uses a FileChannel and various 
optimisations to try and achieve a higher throughput.

Performance
-----------

The milliard variant of the FizzBuzz calculation is a challenge in both
concurrency, high performance IO and processing. We decided to output to
a file, rather than stdout as this gave a higher performance and a more
interesting challenge.

The algorithm is an important part of the design decision. I have opted
for the following 'fixed pattern' approach, where by we make use of the 
fact that position of the Fizz and Buzz doesn't change in the output.

FizzBuzz
1
2
Fizz
4
Buzz
Fizz
7
8
Fizz
Buzz
11
Fizz
13
14
-- FizzBuzz sequence from 0-14

The sequence will then repeat every 15 numbers in the same pattern. There
for a fixed buffer with the pattern in it, and an implementation that slots
the numbers into the pattern should be quite performant.

This fixed pattern approach allows us to use a RandomAccessFile/FileChannel
to allow multi-threaded write access to the same file. Each thread simply
seeks to the location where the block should be written and writes there.