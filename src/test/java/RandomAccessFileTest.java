/**
 * Copyright 2013 ForgeRock AS.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Tests the write performance of a RandomAccessFile.
 *
 * @author Robert Wapshott
 */
public class RandomAccessFileTest {
    private final RandomAccessFile file;

    public RandomAccessFileTest() {
        try {
            file = new RandomAccessFile("badger", "rw");
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }

    public void write(int index, byte[] data) {
        try {
            file.seek(index * data.length);
            file.write(data);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public void close() {
        try {
            file.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public static void main(String[] args) {
        for (int blockSize = 1; blockSize < 50000; blockSize+= 20) {

            byte[] random = generateRandom(blockSize).getBytes();
            long start = System.currentTimeMillis();
            RandomAccessFileTest test = new RandomAccessFileTest();

            for (int ii = 0; ii < 100; ii++) {
                test.write(ii, random);
            }

            long delta = System.currentTimeMillis() - start;
            System.out.println("Wrote " + 100 * random.length + " bytes in " + delta + "ms");
        }
    }

    private static String generateRandom(int size) {
        String r = "";
        for (int ii = 0; ii < size; ii++) {
            r += new Double(Math.random()).toString();
        }
        return r;
    }
}
