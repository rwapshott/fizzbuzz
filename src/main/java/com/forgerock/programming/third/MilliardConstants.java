/**
 * Copyright 2013 ForgeRock AS.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.third;

import com.forgerock.programming.third.patterns.FixedBuffer;

/**
 * Responsible for storing Milliard programming challenge constants.
 *
 * @author Robert Wapshott
 */
public class MilliardConstants {
    /**
     * The size of a batch of patterns. Expressed as the number of
     * patterns that make up the batch.
     */
    public static final int BATCH_SIZE = 400000;
    /**
     * Number of worker threads that will operate at the same time.
     */
    public static final int MAX_THREADS = 5;
    /**
     * The total count of the highest pattern to generate for the programming challenge.
     */
    public static final int TOTAL = 1000000000;

    /**
     * The number of batches that will be stored in the file.
     */
    public static final int TOTAL_BATCHES = TOTAL / FixedBuffer.INCREMENT;

    /**
     * The filename to store the output in.
     */
    public static final String OUTPUT_FILE = "out.tmp";
}
