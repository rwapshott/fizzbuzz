/**
 * Copyright 2013 ForgeRock AS.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.third;

import com.forgerock.programming.third.patterns.FixedBuffer;
import com.forgerock.programming.third.writers.ChannelWriter;

import java.nio.channels.FileChannel;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Responsible for coordinating the threads for the challenge.
 *
 * @author Robert Wapshott
 */
public class Start {

    public Start() {
        CountDownLatch latch = new CountDownLatch(MilliardConstants.MAX_THREADS);
        ExecutorService service = Executors.newFixedThreadPool(MilliardConstants.MAX_THREADS);
        IndexGenerator generator = new IndexGenerator();
        Timing timing = new Timing();

        FileChannel channel = ChannelWriter.getChannel();

        for (int ii = 0; ii < MilliardConstants.MAX_THREADS; ii++) {

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }

            service.execute(new Worker(
                    new ChannelWriter(channel, timing),
                    new FixedBuffer(),
                    generator,
                    latch));
        }

        try {
            latch.await();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        service.shutdown();
    }

    public static void main(String[] arg) {
        long start = System.currentTimeMillis();
        new Start();
        System.out.println("Completed in " + (System.currentTimeMillis() - start) + "ms");
    }
}
