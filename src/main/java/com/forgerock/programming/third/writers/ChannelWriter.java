/**
* Copyright 2013 ForgeRock AS.
*
* The contents of this file are subject to the terms of the Common Development and
* Distribution License (the License). You may not use this file except in compliance with the
* License.
*
* You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
* specific language governing permission and limitations under the License.
*
* When distributing Covered Software, include this CDDL Header Notice in each file and include
* the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
* Header, with the fields enclosed by brackets [] replaced by your own identifying
* information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.third.writers;

import com.forgerock.programming.third.MilliardConstants;
import com.forgerock.programming.third.Timing;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Writes data using a FileChannel implementation. This is slightly faster than
 * the BlockWriter but not by much. The method of accessing it is the same.
 *
 * @author Robert Wapshott
 */
public class ChannelWriter implements Writer {
    private final FileChannel channel;
    private final Timing timing;

    public ChannelWriter(FileChannel channel, Timing timing) {
        this.channel = channel;
        this.timing = timing;
    }

    public static FileChannel getChannel() {
        try {
            RandomAccessFile file = new RandomAccessFile(MilliardConstants.OUTPUT_FILE, "rw");
            FileChannel channel = file.getChannel();
            channel.force(true);
            return channel;
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void write(int index, byte[] data) {
        long token = timing.start();
        try {
            ByteBuffer buffer = ByteBuffer.wrap(data);
            int total = 0;
            while (total != data.length) {
                buffer.position(total);
                total += channel.write(buffer, (long)(index + total));
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        timing.end(token, data.length);
    }

    @Override
    public void close() {
    }
}
