/**
 * Copyright 2013 ForgeRock AS.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.third.writers;

import com.forgerock.programming.third.MilliardConstants;
import com.forgerock.programming.third.Timing;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Initial implementation which handles writing to a RandomAccessFile directly.
 *
 * This implementation works best when data is written in batches.
 *
 * @author Robert Wapshott
 */
public class BlockWriter implements Writer {
    private final RandomAccessFile file;
    private final Timing timing;

    public BlockWriter(Timing timing) {
        this.timing = timing;
        try {
            file = new RandomAccessFile(MilliardConstants.OUTPUT_FILE, "rw");
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }

    public void write(int index, byte[] data) {
        try {
            long token = timing.start();
            file.seek(index);
            file.write(data);
            timing.end(token, data.length);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Close the wrapped file.
     */
    public void close() {
        try {
            file.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
