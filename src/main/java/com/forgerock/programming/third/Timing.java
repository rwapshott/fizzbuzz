/**
 * Copyright 2013 ForgeRock AS.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.third;

import java.util.HashMap;
import java.util.Map;

/**
 * Thread-safe timing code to track the average write speed of the threads.
 *
 * @author Robert Wapshott
 */
public class Timing {
    private long token = 0;
    Map<Long, Long> startedTimers = new HashMap<Long, Long>();
    Map<Long, Integer> completeTimers = new HashMap<Long, Integer>();
    private long internal = System.currentTimeMillis();

    public synchronized long start() {
        token++;
        startedTimers.put(token, System.currentTimeMillis());
        return token;
    }

    public synchronized void end(long token, int bytesWritten) {
        Long start = startedTimers.remove(token);
        long delta = System.currentTimeMillis() - start;
        completeTimers.put(delta, bytesWritten);
        update();
    }

    private void update() {
        if (System.currentTimeMillis() - internal > 1000) {
            float total = 0;
            for (long time : completeTimers.keySet()) {
                total += completeTimers.get(time);
            }
            // to MB
            total /= 1000000;

            System.out.println(total + "mb/s");

            // Reset
            internal = System.currentTimeMillis();
            completeTimers.clear();
        }
    }
}
