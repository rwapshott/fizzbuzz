/**
 * Copyright 2013 ForgeRock AS.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.third;

import com.forgerock.programming.third.patterns.FixedBuffer;

/**
 * Responsible for generating a sequence of indexes for workers to
 * work on. This is best performed in sequential batches to ensure
 * that the output can be collected and stored in batches before
 * writing to disk in batches.
 *
 * @author Robert Wapshott
 */
public class IndexGenerator {
    private int counter = 0;

    /**
     * @return Returns the next number in sequence.
     */
    public synchronized int[] getNext() {
        if (counter > MilliardConstants.TOTAL) return null;

        int[] counters = new int[MilliardConstants.BATCH_SIZE];
        for (int ii = 0; ii < counters.length; ii++) {
            counters[ii] = counter;
            counter += FixedBuffer.INCREMENT;
        }

        return counters;
    }
}
