/**
 * Copyright 2013 ForgeRock AS.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.third;

import com.forgerock.programming.third.patterns.FixedBuffer;
import com.forgerock.programming.third.patterns.Pattern;
import com.forgerock.programming.third.writers.Writer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;

/**
 * Responsible for generating batches of patterns and outputting these to the
 * Writer implementation.
 *
 * A key feature of this implementation is that it retrieves a large batch of
 * contiguous numbers from the IndexGenerator. This ensures that the data that
 * is written is suitable for batching together.
 *
 * @author Robert Wapshott
 */
public class Worker implements Runnable {
    private final Writer writer;
    private final Pattern pattern;
    private final IndexGenerator generator;
    private final CountDownLatch latch;

    private ByteArrayOutputStream buffer = new ByteArrayOutputStream(
            FixedBuffer.getPatternSize() * MilliardConstants.BATCH_SIZE);

    /**
     * @param writer Output for completed batches.
     * @param pattern Generates patterns based on the index.
     * @param generator Generates new batches of pattern indexes.
     * @param latch Required to signal completion of worker.
     */
    public Worker(Writer writer, Pattern pattern,
                  IndexGenerator generator, CountDownLatch latch) {
        this.writer = writer;
        this.pattern = pattern;
        this.generator = generator;
        this.latch = latch;
    }

    @Override
    public void run() {
        int[] counters = new int[]{};
        while (counters != null) {
            counters = generator.getNext();
            if (counters == null) continue;

            fillBuffer(counters);
            writeBuffer(counters[0]);
            buffer.reset();
        }

        writer.close();
        latch.countDown();
    }

    /**
     * Fill the buffer with the patten blocks, which are known to be in
     * sequential order.
     *
     * @param counters Counters to fill the buffer with.
     */
    private void fillBuffer(int[] counters) {
        for (int counter : counters) {
            try {
                buffer.write(pattern.generate(counter).getBytes());
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    /**
     * Write the buffer to the disk.
     *
     * @param batchStart The index of the first pattern in the buffer.
     */
    private void writeBuffer(int batchStart) {
        writer.write(batchStart, buffer.toByteArray());
    }
}
