/**
 * Copyright 2013 ForgeRock AS.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.third.patterns;

import java.util.Arrays;

/**
 * An alternative implementation which uses string buffers.
 *
 * @author Robert Wapshott
 */
public class StringBufferPattern implements Pattern {
    private final byte[] EMPTY = new byte[10];

    public String generate(int index) {
        StringBuffer buffer = new StringBuffer();
        Arrays.fill(EMPTY, (byte)' ');

        buffer.append("FizzBuzz\n");
        number(buffer, index, 1);
        number(buffer, index, 2);
        buffer.append("Fizz\n");
        number(buffer, index, 4);
        buffer.append("Buzz\n");
        buffer.append("Fizz\n");
        number(buffer, index, 7);
        number(buffer, index, 8);
        buffer.append("Fizz\n");
        buffer.append("Buzz\n");
        number(buffer, index, 11);
        buffer.append("Fizz\n");
        number(buffer, index, 13);
        number(buffer, index, 14);

        return buffer.toString();
    }

    private void number(StringBuffer buffer, int index, int offset) {
        byte[] number = Integer.toString(index + offset).getBytes();
        System.arraycopy(number, 0, EMPTY, 0, number.length);
        buffer.append(new String(EMPTY));
        buffer.append("\n");
    }

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        StringBufferPattern generator = new StringBufferPattern();
        for (int ii = 0; ii < 1000000; ii+= 15) {
            String generate = generator.generate(ii);
            System.out.print(generate);
        }
        System.out.println("Complete in " + (System.currentTimeMillis() - start) + "ms");
    }
}
