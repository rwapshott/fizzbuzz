/**
 * Copyright 2013 ForgeRock AS.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.third.patterns;

/**
 * Responsible for modelling a fixed size buffer containing the
 * FizzBuzz pattern.
 *
 * This implementation is trying to avoid allocating any additional
 * memory and is mostly successful in this. The generation of the
 * numbers is the only new allocation.
 *
 * @author Robert Wapshott
 */
public class FixedBuffer implements Pattern {
    private final char[] PATTERN =
            ("FizzBuzz\n" +
            "          \n" +
            "          \n" +
            "Fizz\n" +
            "          \n" +
            "Buzz\n" +
            "Fizz\n" +
            "          \n" +
            "          \n" +
            "Fizz\n" +
            "Buzz\n" +
            "          \n" +
            "Fizz\n" +
            "          \n" +
            "          \n").toCharArray();

    /**
     * Matrix of offsets to apply to the pattern:
     * [start char][offset amount]
     */
    private static final int[][] increment = new int[][] {
        new int[]{9, 1},
        new int[]{20, 2},
        new int[]{36, 4},
        new int[]{57, 7},
        new int[]{68, 8},
        new int[]{89, 11},
        new int[]{105, 13},
        new int[]{116, 14}
    };

    /**
     * The number of numbers that are included in each block.
     */
    public static final int INCREMENT = 15;

    /**
     * @return The size of each block in bytes.
     */
    public static int getPatternSize() {
        String r = new String(new FixedBuffer().PATTERN);
        return r.getBytes().length;
    }

    /**
     * @param index A factor of 15 which represents the starting point in the sequence.
     * @return A String containing the formatted multi-line FizzBuzz output.
     */
    public String generate(int index) {
        for (int[] apply : increment) {
            char[] number = String.valueOf(index + apply[1]).toCharArray();
            System.arraycopy(number, 0, PATTERN, apply[0], number.length);
        }
        return new String(PATTERN);
    }

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        FixedBuffer fixedBuffer = new FixedBuffer();
        for (int ii = 0; ii < 1000000; ii+= 15) {
            fixedBuffer.generate(ii);
            System.out.print(fixedBuffer.generate(ii));
        }
        System.out.println("Complete in " + (System.currentTimeMillis() - start) + "ms");
    }
}
