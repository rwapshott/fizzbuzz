package com.forgerock.programming.first;

import java.text.MessageFormat;

/**
 * A simple demonstration of a FizzBuzz algorithm that outputs to 1,000,000.
 *
 * @author Robert Wapshott
 */
public class Million {
    public static void main(String[] args) {
        long start = System.currentTimeMillis();

        StringBuilder line = new StringBuilder();
        boolean added = false;

        for (int ii = 0; ii < 1000000; ii++) {
            if (ii % 3 == 0) {
                line.append("Fizz");
                added = true;
            }
            if (ii % 5 == 0) {
                line.append("Buzz");
                added = true;
            }
            if (!added) line.append(ii);
            line.append('\n');
            added = false;
        }
        System.out.println(line.toString());

        System.out.println(MessageFormat.format("Took {0}ms", System.currentTimeMillis() - start));
    }
}