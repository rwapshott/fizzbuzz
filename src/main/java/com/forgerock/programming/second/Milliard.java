/**
 * Copyright 2013 ForgeRock AS.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.second;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * FizzBuzz to a milliard is a harder problem because of the volume of data to deal with.
 *
 * In this case, we will be using all cores on the machine to process the more results
 * and then coordinating the adding of these to the queue to ensure they are written to
 * disk in the right order.
 *
 * @author Robert Wapshott
 */
public class Milliard {

    private static final int INCREMENT = 100000;
    private static final int TOTAL = 1000000000;
    private static final int MAX_WORKERS = 7;

    private final ExecutorService service = Executors.newFixedThreadPool(MAX_WORKERS + 1);
    private final Printer printer = new Printer();
    private List<Worker> workers = new ArrayList<Worker>();
    private int counter = 0;
    private CountDownLatch latch;

    public Milliard() {
        service.execute(printer);

        while (!isCounterExceeded()) {
            workers.clear();

            fillWorkers();

            for (Worker worker : workers) {
                service.execute(worker);
            }

            try {
                latch.await();
            } catch (InterruptedException e) {
                e.printStackTrace(System.err);
                return;
            }

            for (Worker worker : workers) {
                printer.appendOutput(worker.getOutput());
            }
        }

        service.shutdownNow();

        while (!printer.isFinished()) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    private void fillWorkers() {
        List<Integer> tasks = new ArrayList<Integer>();
        int next = 0;
        while (tasks.size() < MAX_WORKERS && next != -1) {
            next = getNext();
            tasks.add(next);
        }
        latch = new CountDownLatch(tasks.size());
        for (int ii = 0; ii < tasks.size(); ii++) {
            int t = tasks.get(ii);
            workers.add(new Worker(t, t + INCREMENT, latch));
        }
    }

    private boolean isCounterExceeded() {
        return counter > TOTAL;
    }

    private int getNext() {
        int r = counter;
        counter += INCREMENT;
        if (isCounterExceeded()) {
            return -1;
        }
        return r;
    }

    private class Worker implements Runnable {

        private final int start;
        private final int end;
        private final CountDownLatch countdown;
        private StringBuilder line;

        public Worker(int start, int end, CountDownLatch countdown) {
            this.start = start;
            this.end = end;
            this.countdown = countdown;
        }

        @Override
        public void run() {
            line = new StringBuilder();
            boolean added = false;

            for (int ii = start; ii < end; ii++) {
                if (ii % 3 == 0) {
                    line.append("Fizz");
                    added = true;
                }
                if (ii % 5 == 0) {
                    line.append("Buzz");
                    added = true;
                }
                if (!added) line.append(ii);
                line.append("\n");
                added = false;
            }

            countdown.countDown();
        }

        public String getOutput() {
            return line.toString();
        }
    }

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        new Milliard();
        System.out.println("Complete in " + (System.currentTimeMillis() - start) + "ms");
    }
}
