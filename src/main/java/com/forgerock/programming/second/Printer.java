/**
 * Copyright 2013 ForgeRock AS.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.second;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
* Responsible for queuing and printing the output.
*
* @author Robert Wapshott
*/
public class Printer implements Runnable {
    private BufferedOutputStream tmp;
    private final BlockingQueue<byte[]> print = new ArrayBlockingQueue<byte[]>(50);
    private boolean finished = false;

    private Timing timing = new Timing();

    Printer() {
        try {
            tmp = new BufferedOutputStream(new FileOutputStream("out.tmp", false));
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }

    public void appendOutput(String line) {
        print.offer(line.getBytes());
    }

    @Override
    public void run() {
        while (!isFinished()) {
            try {
                timing.timeStart();
                byte[] take = print.take();
                tmp.write(take);
                timing.timeEnd(take);
            } catch (InterruptedException e) {
                finished = true;
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
        try {
            tmp.flush();
            tmp.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public boolean isFinished() {
        return finished && print.isEmpty();
    }
}
