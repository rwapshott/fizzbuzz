/**
 * Copyright 2013 ForgeRock AS.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package com.forgerock.programming.second;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
* Responsible for timing metrics.
*
* @author Robert Wapshott
*/
public class Timing {
    // Metrics
    private List<Long> time = new ArrayList<Long>();
    private List<Long> speed = new ArrayList<Long>();

    long seconds = System.currentTimeMillis();
    long timer;

    public void timeStart() {
        if (hasTimerExpired()) {
            System.out.println(format());
            reset();
        }
        timer = System.currentTimeMillis();
    }

    public void timeEnd(byte[] data) {
        time.add(System.currentTimeMillis() - timer);
        speed.add(Long.valueOf(data.length));
    }

    private boolean hasTimerExpired() {
        return System.currentTimeMillis() - seconds > 1000;
    }

    private String format() {
        float total = 0;
        for (long s : speed) {
            total += s;
        }
        total /= 1000000;

        return MessageFormat.format(
                "Write Metrics: Avg Time: {0}ms  Avg Speed {1}mb/s",
                avg(time),
                total);
    }

    private void reset() {
        time.clear();
        speed.clear();
        seconds = System.currentTimeMillis();
    }

    private float avg(List<Long> values) {
        float avg = 0;
        for (Long time : values) {
            avg += time;
        }
        return avg / values.size();
    }
}
